<?php

namespace Database\Factories;

use App\Models\User;
use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile as HttpUploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Feature\AuthenticationTest;
use Tests\Feature\PostTest;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        Storage::fake('local');
        $file = HttpUploadedFile::fake()->create('file.png');

        return [
            'title' => $this->faker->sentence(),
            'subject' => $this->faker->sentence(),
            // 'image' => 'https://source.unsplash.com/random',
            'image' => $file,
            'user_id' => '1',
        ];
    }
}
