<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();
        // Schema::enableForeignKeyConstraints();
        $users = [
            'name'     =>  'Hardik Kanzariya',
            'email'     =>  'hardik.kanzariya@yudiz.com',
            'password'     =>  Hash::make('hardik123'),
        ];

        User::create($users);
    }
}
