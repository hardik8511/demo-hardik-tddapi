<?php

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('userLogin', [UserController::class, 'UserLogin'])->name('user.userLogin');
Route::middleware('auth:sanctum')->group(function () {

    Route::controller(PostController::class)->group(function () {
        Route::post('create_post',  'store')->name('user.create_post');
        Route::post('updatePost',  'update')->name('user.updatePost');
        Route::post('deletePost',  'destroy')->name('user.deletePost');
    });
});
