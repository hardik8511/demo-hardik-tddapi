<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;
    public function authUser()
    {
        return Sanctum::actingAs(
            User::factory()->create(),
        );
    }

    public function createPost()
    {
        return Post::factory()->create();
    }

    public function test_only_login_user_create_post()
    {
        $response =  $this->postJson(route('user.create_post'), ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    public function test_create_post_image_required()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();
        $post_data = Post::factory()->create(['user_id' => $user_data->id]);
        $data = ['title' => $post_data->title, 'subject' => $post_data->subject];
        $response =  $this->postJson(route('user.create_post'), $data, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function test_create_post_image_mimes_not_valid()
    {
        $this->withoutExceptionHandling();
        Storage::fake('local');
        $file = UploadedFile::fake()->create('file.pdf');
        $user_data = $this->authUser();
        $post_data = Post::factory()->create(['image' => $file, 'user_id' => $user_data->id]);

        $data = ['title' => $post_data->title, 'subject' => $post_data->subject, 'image' => $file];
        $response =  $this->postJson(route('user.create_post'), $data, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }



    // public function test_post_create_successfully()
    // {
    //     $this->withoutExceptionHandling();
    //     Storage::fake('pubilc');
    //     $file = UploadedFile::fake()->create('file.png');
    //     $this->authUser();
    //     $post_data = Post::factory()->create();

    //     $data = ['title' => 'test cases test test testing test tetess', 'subject' => 'test cases subject', 'image' => $file];
    //     $response =  $this->postJson(route('user.create_post'), $data, ['Accept' => 'application/json']);
    //     $response->assertStatus(Response::HTTP_CREATED);
    //     $response->assertJsonStructure([
    //         "data" => [
    //             "title",
    //             "subject",
    //             "image"
    //         ],
    //         "meta" => [
    //             "message"
    //         ]
    //     ]);
    // }


    // delete post test cases start 
    public function test_when_delete_but_post_is_not_found_in_db()
    {
        $this->withoutExceptionHandling();
        $user_data = $this->authUser();
        $post_data =  Post::factory()->create(['id' => 2, 'user_id' => $user_data->id]);
        // dd($post_data);
        $response =  $this->postJson(route('user.deletePost'), ['post_id' => "10"], ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "post records not found "
            ]
        ]);
    }

    public function test_can_not_delete_another_user_post()
    {
        $this->withoutExceptionHandling();
        // $this->authUser();
        $this->authUser();
        $user_data = User::factory()->create(['id' => 3]);

        $post_data = Post::factory()->create(['user_id' =>  $user_data->id]);
        // dd($post_data);
        $response =  $this->postJson(route('user.deletePost'), ['post_id' => $post_data->id], ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "you can not delete the anohther user post"
            ]
        ]);
    }

    public function test_post_delete_sucessfully()
    {
        $this->withoutExceptionHandling();
        $user_data = $this->authUser();
        $post_data = Post::factory()->create(['user_id' => $user_data->id]);

        $response =  $this->postJson(route('user.deletePost'), ['post_id' =>  $post_data->id], ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "post deleted sucessfully"
            ]
        ]);
    }
    //delete post test cases end 


    //edit post test cases start 
    public function test_edit_post_user_not_authenticate()
    {
        $user_data = User::factory()->create();
        $post_data = Post::factory()->create(['user_id' => $user_data->id]);
        $response =  $this->postJson(route('user.updatePost'), ['post_id' => $post_data->id], ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_user_edit_post_not_found()
    {
        $this->withoutExceptionHandling();
        $user_data = $this->authUser();
        $post_data = Post::factory()->create(['id' => 5, 'user_id' => $user_data->id]);

        // dd($is_login);
        $post_data = [
            'title' =>  $post_data->title,
            'subject' => $post_data->subject,
            'post_id' => "1",
        ];

        $response =  $this->postJson(route('user.updatePost'), $post_data, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "post data is not found"
            ]
        ]);
        // dd($response);
    }
    public function test_user_not_edit_another_user_post()
    {
        $this->withoutExceptionHandling();
        $user_data_auth =  $this->authUser();
        $user_data =  User::factory()->create();
        $post_data = Post::factory()->create(['user_id' => $user_data->id]);
        $post_data2 = Post::factory()->create(['user_id' => $user_data_auth->id]);
        // dd($post_data2->id);

        $post_data = [
            'title' => $post_data2->title,
            'subject' => $post_data2->subject,
            'post_id' => $post_data->id
        ];
        $response =  $this->postJson(route('user.updatePost'),  $post_data, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "you can not edit the another user post"
            ]
        ]);
    }


    public function test_post_edit_sussfully()
    {
        $this->withoutExceptionHandling();
        $this->authUser();

        $post_data = Post::factory()->create(['user_id' => $this->authUser()->id]);


        $post_data = [
            'title' => 'update title post',
            'subject' => 'update subject post',
            'post_id' => $post_data->id
        ];
        $response =  $this->postJson(route('user.updatePost'),  $post_data, ['Accept' => 'application/json']);
        $response->assertStatus(Response::HTTP_OK);
    }

    //edit post test cases end 

}
