<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    // public function test_is_login_route_found()
    // {
    //     $this->withoutExceptionHandling();
    //     $response = $this->postJson(route('user.userLogin'))->assertStatus(403);
    //     // dd($response);
    // }

    public static function user_create()
    {

        return User::factory()->create();
    }

    public function test_email_is_required()
    {
        $this->withoutExceptionHandling();
        $response =  $this->postJson(route('user.userLogin'), [], ['Accept' => 'application/json']);
        $response->assertStatus(422)->assertJson([
            "data" => null,
            "meta" => [
                "message" => "The email field is required."
            ]
        ]);
    }

    public function test_password_is_required()
    {
        $user_data =  $this->user_create();
        $data = [
            'email' => $user_data->email,
            'password' => '',
        ];
        $this->withoutExceptionHandling();
        $this->postJson(route('user.userLogin'), $data, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertUnprocessable()->assertJson([
                "data" => null,
                "meta" => [
                    "message" => "The password field is required.",
                ]
            ]);
    }


    public function test_email_is_not_valid()
    {
        $user_data = User::factory()->create(['email' => "abcdd@"]);
        $data = [
            'name' => $user_data->name,
            'email' => $user_data->email,
        ];
        $response =  $this->postJson(route('user.userLogin'),  $data, ['Accept' => 'application/json'])
            ->assertStatus(422)->assertJson([
                "data" => null,
                "meta" => [
                    "message" => "The email must be a valid email address."

                ]
            ]);
    }

    public function test_invalid_credential()
    {
        // $this->withoutExceptionHandling();
        $user_data = $this->user_create();
        $login_data = ['email' => $user_data->email, 'password' => 'hardik123'];

        $response =  $this->postJson(route('user.userLogin'), $login_data, ['Accept' => 'application/json']);

        $response->assertStatus(422); //invalid password not login 
        $response->assertJson([
            "data" => null,
            "meta" => [
                "message" => "invalid credential",
            ]
        ]);
        $response->assertJsonStructure([
            "data" => [],
            "meta" => [
                "message"
            ]
        ]);
    }

    public function test_login_sucess()
    {
        $this->withoutExceptionHandling();
        $user_data = self::user_create();
        $login_data = ['email' =>  $user_data->email, 'password' => "password"];
        $response =  $this->postJson(route('user.userLogin'), $login_data, ['Accept' => 'application/json']);
        // dd($response);
        $response->assertStatus(200);
        $response->assertJson([
            "data" => [
                'name' => $user_data->name,
                'email' => $user_data->email,
            ],
            "meta" =>  [
                "message" =>  "login sucessfully"
            ]
        ]);
        $response->assertJsonStructure([
            "data" => [
                'name',
                'email',
            ],
            "meta" => [
                "message"
            ]
        ]);
    }
}
