<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Sanctum\Sanctum;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    
    // public function createUser()
    // {   
    //     return User::factory()->create();
    // }
    // public function authUser()
    // {   
    //     // dd("hi auth user call");
    //     $user = $this->createUser();
    //     // dd($user);
    //     Sanctum::actingAs($user);
    //     return $user;
    // }
}
