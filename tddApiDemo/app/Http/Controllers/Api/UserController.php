<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserLoginResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function UserLogin(Request $request)
    {
        // dd($request->input());
        // dd("hiii");s
        $rules = [
            'email' => 'required|min:5|max:150|email',
            'password' => 'required|min:8|max:16',
        ];
        // dd("ihii");
        if ($this->apiValidator($request->all(), $rules)) {
            $userdata = array(
                'email' =>  $request->email,
                'password' => $request->password
            );
            try {
                $user = User::where('email', $request->email)->first();
                // dd($user);
                if (isset($user)) {
                    if (Hash::check($request->password, $user->password) == 'false') {
                        Auth::login($user);
                        // dd($user);
                        return (new UserLoginResource($user))
                            ->additional([
                                'meta' => [
                                    'message' =>  trans('api.login'),
                                ]
                            ])
                            ->response()
                            ->header('X-Auth-Token', $user->createToken('laravel')->plainTextToken);
                    } else {
                        // dd("hi password is false ");
                        $this->response['meta']['message']  = trans('api.login_fail');
                    }
                } else {
                    $this->status = Response::HTTP_UNAUTHORIZED;
                    $this->response['meta']['message']  = trans('api.login_fail');
                }
            } catch (\Exception $e) {
                $this->response['meta']['message'] = $e->getMessage();
            }
        }
        // else {
        //     $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
        //     $this->response['meta']['message']  = trans('api.login_fail');
        // }
        return $this->returnResponse();
    }
}
