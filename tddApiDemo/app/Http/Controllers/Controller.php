<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function apiValidator($fields, $rules, $message = array())
    {
        $validator = Validator::make($fields, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $this->response['meta']['message'] = $errors->first();
            return false;
        }
        return true;
    }

    protected $response = array('data' => null);
    protected $status = Response::HTTP_UNPROCESSABLE_ENTITY;
    public function returnResponse()
    {
        return response()->json($this->response, $this->status);
    }
}
