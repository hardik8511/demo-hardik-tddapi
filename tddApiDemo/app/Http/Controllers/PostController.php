<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResources;
use App\Models\Post;
use Dotenv\Validator;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input());
        // dd(auth()->user()->id);
        $rules = [
            'title' => 'required|min:5|max:150|unique:posts',
            'subject' => 'required|min:5|max:150',
            'image' => 'required|mimes:jpeg,jpg,png'
        ];

        $validator = FacadesValidator::make($request->all(), $rules);
        if ($validator->fails()) {
            $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->response['meta']['message'] = $validator->errors()->first();
            return $this->returnResponse();
        }

        try {
            $path = null;
            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('postImage');
            }
            $post_data =  Post::create([
                'title' => $request->title,
                'subject' => $request->subject,
                'image' => $path,
                'user_id' => auth()->user()->id
            ]);

            return (new PostResources($post_data))->additional([
                'meta' => [
                    'message' => "Post Created Successfully"
                ]
            ]);
            // $this->response['meta']['message'] = "Post Created Successfully";


        } catch (\Exception $e) {
            $this->response['meta']['message'] = $e->getMessage();
        }


        return $this->returnResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        // dd($request->post_id);
        $rules = [
            'title' => 'required|min:5|max:150',
            'subject' => 'required|min:5|max:150',
            'image' => 'nullable|mimes:jpeg,jpg,png'
        ];

        $validator = FacadesValidator::make($request->all(), $rules);
        if ($validator->fails()) {
            $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
            return $this->returnResponse();
        }

        try {
            $post_data = Post::find($request->post_id);
            if (empty($post_data)) {
                // dd("hii ippo");
                $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $this->response['meta']['message'] = "post data is not found";
                return $this->returnResponse();
            }

            if (auth()->user()->id != $post_data->user_id) {
                // dd("hiii");
                $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $this->response['meta']['message'] = "you can not edit the another user post";
                return $this->returnResponse();
            }

            // dd("hii");

            $path = null;
            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('postImage');
            }
            // dd($request->post_id);
            // $post_data = Post::find();
            $post_data->title = $request->title;
            $post_data->subject = $request->subject;
            $post_data->image = $path ? $path : $post_data->image;
            $post_data->save();

            return (new PostResources($post_data))->additional([
                'meta' => [
                    'message' => "Post Updated Successfully"
                ]
            ]);
            // dd($data);
        } catch (QueryException $e) {
            // dd("hiii");
            $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
            $this->response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $this->response['meta']['message'] = $e->getMessage();
        }

        return $this->returnResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->input());
        // dd(auth()->user()->id);
        try {
            $post_data = Post::find($request->post_id);
            // dd($post_data);
            if ($post_data) {
                if ($post_data->user_id == auth()->user()->id) {
                    $post_data->delete();
                    $this->status = Response::HTTP_OK;
                    $this->response['meta']['message'] = 'post deleted sucessfully';
                } else {
                    $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
                    $this->response['meta']['message'] = 'you can not delete the anohther user post';
                }
            } else {
                $this->status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $this->response['meta']['message'] = 'post records not found ';
            }
        } catch (Exception $e) {
            $this->response['meta']['message'] = $e->getMessage();
        }
        return $this->returnResponse();
    }
}
