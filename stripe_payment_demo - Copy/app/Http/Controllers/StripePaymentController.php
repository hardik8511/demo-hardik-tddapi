<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Customer as StripeCustomer;
use Stripe\PaymentIntent;
use Stripe\Service\PaymentIntentService;
use Stripe\Stripe;
use Stripe\StripeClient;

class StripePaymentController extends Controller
{

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }
    //
    public function createCustmoer()
    {
    }

    public function checkout()
    {
        return view('stripe.welcome');
    }

    public function checkOutPost(Request $request)
    {
        // dd($request->input());
        // Stripe::setApiKey(env('STRIPE_SECRET'));

        //stripe cutomer create 
        // $customer = StripeCustomer::create(array(
        //     'name' => 'Test Name',
        //     'email' => $request->stripeEmail,
        //     'source'  => $request->stripeToken,
        //     // 'created' => time(),
        // ));

        $customer = StripeCustomer::create(array(
            'name' => 'Test Name',
            'email' => $request->stripeEmail,
            'source'  => $request->stripeToken,
            // 'created' => time(),
        ));
        // dd($customer->id);
        // Charge::create([
        //     "amount" => 100 * 100,
        //     "currency" => "usd",
        //     "source" => $request->stripeToken,
        //     "description" => "Test payment from create the hcarge .",
        // ]);

        //insert into over local database 
        $customer_data = Customer::create([
            'stripe_id' => $customer->id,
            'stripe_token' => $request->stripeToken,
            'name' => $customer->name,
            'phone' => $customer->phone,
            'email' => $customer->email,
        ]);




        // dd("c");
        //stripe customer retrive 
        // if( $customer && $customer->id )
        // {
        //     $client->stripe_id = $result->id;
        //     $client->stripe_token = $stripe_token;
        //     $client->save();
        // }
        // dd($customer);
    }

    public function retrieveCustomer(Request $request)
    {
        $customer_data =  StripeCustomer::retrieve('cus_LcrK4bPb4KWGvo');
        //    dd($customer_data);
    }

    public function createPayment(Request $request)
    {
        $create_payment = PaymentIntent::create([
            'amount' => 2000,
            'currency' => 'usd',
            'payment_method_types' => ['card'],
        ]);

        // dd($create_payment);
    }


    //another demo for the stripe 
    public function stripe()
    {
        return view('stripe.stripe');
    }
    public function stripePost(Request $request)
    {
        // dd($request->input());
        // Charge::create([
        //     "amount" => 100 * 100,
        //     "currency" => "usd",
        //     "source" => $request->stripeToken,
        //     "description" => "Test payment from itsolutionstuff.com."
        // ]);

        $create_payment = PaymentIntent::create([
            'amount' => 2000,
            'currency' => 'usd',
            'payment_method' => 'card',
            'payment_method_types' => ['card'],
        ]);

        Session::flash('success', 'Payment successful!');

        return back();
    }

    public function confirmPayment()
    {

        // $confirm_data =   PaymentIntent::confirm([
        //     'id' => 'pi_1Dr1Te2eZvKYlo2CKRiHyME9',
        // ]);
        // $stripe = new StripeClient(env('STRIPE_SECRET'));
        // // dd($stripe);
        // $payment_intent = new PaymentIntent('pi_3KvfXnSBhK91ejUZ0PXHG8I2');
        // // dd($payment_intent);
        // $confirm_data =   $payment_intent->paymentIntents->confirm(
        //     'pi_3KvfXnSBhK91ejUZ0PXHG8I2',
        // );

        // // $confirm_data = PaymentIntent::retrieve([
        // //     "id" => 'pi_3KvfXnSBhK91ejUZ0PXHG8I2'
        // // ], ['payment_method' => 'card']);
        // dd($confirm_data);

    }

    public function retrivePaymentIntentById()
    {
        $payment_intent_data = PaymentIntent::retrieve([
            "id" => 'pi_3KvfXnSBhK91ejUZ0PXHG8I2'
        ], []);
        if ($payment_intent_data) {
            dd($payment_intent_data);
        } else {
            dd("not found any payment intent");
        }
    }


    public function retriveAllPaymentIntent()
    {
        $payment_data = PaymentIntent::all();
        dd($payment_data['data']);
    }

    // public function createCharge(Request $request)
    // {

    // }

    //using the webhook fetch the payment sucess 
    public function paymentSucess()
    {
    }
}
