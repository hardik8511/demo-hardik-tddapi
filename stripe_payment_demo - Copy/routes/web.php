<?php

use App\Http\Controllers\StripePaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('check-out', [StripePaymentController::class, 'checkOut'])->name('checkout');
Route::get('retrive-customer', [StripePaymentController::class, 'retrieveCustomer'])->name('retrieveCustomer');
Route::post('check-out-post', [StripePaymentController::class, 'checkOutPost'])->name('checkout.post');

//payment intent 
// Route::get('check-out',[StripePaymentController::class,'checkOut'])->name('checkout');


//stripe another demoo 
Route::get('stripe', [StripePaymentController::class, 'stripe']);
Route::post('stripe', [StripePaymentController::class, 'stripePost'])->name('stripe.post');
Route::get('retrive-payment-intent',[StripePaymentController::class,'retrivePaymentIntentById'])->name('retrivePaymentIntentById');
Route::get('confirm-payment-intent',[StripePaymentController::class,'confirmPayment'])->name('confirmPayment');
// retrive all payment intent
Route::get('retrive-all-payment',[StripePaymentController::class,'retriveAllPaymentIntent'])->name('retriveAllPaymentIntent');