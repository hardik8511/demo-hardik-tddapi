<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Factories\UserFactory;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response as HttpResponse;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class StripePaymentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function authUser($is_custom_field = "")
    {
        return  Sanctum::actingAs(
            User::factory()->create($is_custom_field),
        );
    }
    public function test_create_customer_but_not_unauthenticated()
    {
        // $this->withoutExceptionHandling();
        $user_data = User::factory()->create();
        $data = [
            'name' => $user_data->name,
            'email' => $user_data->email,
        ];
        $response =  $this->postJson(route('createCustomer'), $data, ['Accept' => 'application/json']);
        $response->assertStatus(HttpResponse::HTTP_UNAUTHORIZED);
    }

    public function test_create_customer_email_is_required()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();

        // dd($user_data);

        $data = [
            'name' => $user_data->name,
            'email' => "",
        ];
        $response =  $this->postJson(route('createCustomer'), $data, ['Accept' => 'application/json']);
        $response->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }


    public function test_create_customer_email_already_register_in_stripe()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();
        $data = [
            'name' => $user_data->name,
            'email' => "hardikk@yudiz.com",
        ];

        $response =  $this->postJson(route('createCustomer'), $data, ['Accept' => 'application/json']);
        $response->assertStatus(HttpResponse::HTTP_CONFLICT);
    }

    public function test_create_customer_sucesss()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();
        $data = [
            'name' => $user_data->name,
            'email' => $user_data->email,
        ];

        $response =  $this->postJson(route('createCustomer'), $data, ['Accept' => 'application/json']);
        // dd($response);
        $response->assertStatus(HttpResponse::HTTP_OK);
    }
    //customer test cases end 


    //start for ephmeralkeys
    public function test_user_is_not_autheticate_to_create_ephmeralkeys()
    {
        // $this->withoutExceptionHandling();
        $response =  $this->postJson(route('createEphemeralKey'), [], ['Accept' => 'application/json']);
        // dd($response);
        $response->assertStatus(HttpResponse::HTTP_UNAUTHORIZED);
    }
    public function test_ephmeralkeys_customer_not_retrive()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();
        $data =  [];
        $response =  $this->postJson(route('createEphemeralKey'), [], ['Accept' => 'application/json']);
        // dd($response);
        $response->assertStatus(HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function test_epmeralkey_keys_sucessfully()
    {
        $this->withoutExceptionHandling();
        $user_data =  $this->authUser();
        $data = [
            'name' => $user_data->name,
            'email' => $user_data->email,
        ];

        $response_stripe =  $this->postJson(route('createCustomer'), $data, ['Accept' => 'application/json']);
        $customer_id = $response_stripe['data']['id'];
        // dd($customer_id);
        $data =  [
            'customer_id' => $customer_id,
            'stripe_version' => "2020-08-27",
        ];
        $response =  $this->postJson(route('createEphemeralKey'), $data, ['Accept' => 'application/json']);

        $response->assertStatus(HttpResponse::HTTP_OK);
    }

    

}
