<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserLoginResorce;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    public function userLogin(Request $request)
    {
        $response = [];
        // dd($request->input());
        $rules =  [
            'email' => 'required|email|min:5|max:100',
            'password' => 'required|min:8'
        ];
        $validation = Validator::make($request->all(), $rules,);
        if ($validation->fails()) {
            $status     = Response::HTTP_UNPROCESSABLE_ENTITY;
            $response['message']    = $validation->errors()->first();
            $response['data']       = null;

            return response()->json($response, $status);
        }

        $credential = ['email' => $request->email, 'password' => $request->password];
        $user_data = User::where('email', '=', $request->email)->first();
        // $user_data = Auth::guard('web')->attempt($credential);
        // dd($user_data);
        if ($user_data) {
            if (Hash::check($request->password, $user_data->password)) {
                if ($user_data->stripe_id == null) {
                    StripePaymentController::createCustomer($user_data);
                }
                // dd($user_data);
                $token = $user_data->createToken("user_login")->plainTextToken;
                // $token = "123";
                // dd($token);
                return (new UserLoginResorce($user_data))->additional([
                    'meta' => [
                        'message' => "User Login Sucessfully"
                    ]
                ])->response()
                    ->header('X-Custom-Token', $token);
            } else {
                $response['data'] = null;
                $response['meta']['message'] = "Invalid Credential";
                return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        // $user_data = User::
    }
}
