<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TransactionModel;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Stripe\Customer;
use Stripe\Stripe;

class StripePaymentController extends Controller
{
    //
    public $stripe;
    public $status;
    public $response;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );

        // dd(config('services.stripe.secret'));
        // Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public static function createCustomer(User $user)
    {
        // dd($user);
        // dd(env('STRIPE_SECRET'));
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $response = [];
        try {
            $customer = \Stripe\Customer::create(
                [
                    'email'     =>      $user->email,
                    'name'     =>      $user->name,
                    'metadata'  =>  [
                        'first_name'    =>  $user->first_name,
                        'last_name'     =>  $user->last_name,
                    ],
                ]
            );

            // dd($customer);
            $user->stripe_id = $customer->id;
            $user->save();
            $response['status']     = 'success';
            $response['message']    = 'Customer created successfully!';
            $response['data']       = $customer;
        } catch (Exception $exception) {
            $response['status']     = 'error';
            $response['message']    = 'Error to add customer!';
            $response['data']       = $exception;
        } finally {
            return (object) $response;
        }
    }

    public static function createCustomerManually(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'name' => 'required',
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json($validation->errors()->first(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $response = [];
        try {
            //if customer email is already then not create the custoemr 
            $is_customer =  Customer::all(["email" => $request->email]);
            // dd($is_customer['data']);
            if ($is_customer['data']) {
                // dd("hiiii");
                $status = Response::HTTP_CONFLICT;
                $response['data']       = null;
                $response['meta']['message'] = "customer is already exists by this email id ";
                return response()->json($response, $status);
            }


            $customer = \Stripe\Customer::create(
                [
                    'email'     =>      $request->email,
                    'name'     =>      $request->name,
                ]
            );

            $user_data = User::find(Auth::user()->id);
            $user_data->stripe_id = $customer->id;
            $user_data->save();

            if ($customer) {
                $status = Response::HTTP_OK;
                $response['data']       = $customer;
                $response['meta']['message']    = 'Customer created successfully!';
            } else {
                $status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $response['data']       = null;
                $response['meta']['message'] = "something is wrong";
            }
        } catch (Exception $exception) {
            $status = Response::HTTP_UNPROCESSABLE_ENTITY;
            $response['data']       = null;
            $response['meta']['message']    = $exception->getMessage();
        }
        return response()->json($response, $status);
    }

    public function createEphemeralKey(Request $request)
    {
        // dd(Auth::user());
        // dd(Auth::user()->stripe_id);
        $customer = Customer::retrieve(Auth::user()->stripe_id == null ? $request->customer_id : Auth::user()->stripe_id);

        $key = \Stripe\EphemeralKey::create(
            ['customer' => $customer],
            ['stripe_version' => $request->stripe_version]
        );

        $this->stripe = $key;
        $this->status = Response::HTTP_OK;
        return response()->json($this->stripe, $this->status);
    }

    //payment Intent
    public function createPaymentIntent(Request $request)
    {
        // dd($request->input());
        $rules = [
            'amount' => 'required',
            'currency' => 'required',
            'payment_type' => 'required|in:reservation,security',
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json($validation->errors()->first(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $intent = \Stripe\PaymentIntent::create([
            'customer'      =>  Auth::user()->stripe_id,
            'amount' => $request->amount,
            'currency' => $request->currency,
        ]);

        $client_secret = $intent->client_secret;

        $transaction = TransactionModel::create([
            'user_id' => auth()->user()->id,
            'intent_id' => $intent->id,
            'amount' => $request->amount,
            'currency' => $request->currency,
            'payment_type' => $request->payment_type,
            'status' => 'pending',
            'type' => 'payment',
        ]);

        $this->response['data'] = $client_secret;
        $this->status = Response::HTTP_OK;
        $this->response['meta']['message']  =  "Payment Intent Key Retrieved Successfully!";

        return response()->json($this->response, $this->status);
    }

    //payment sucess web hook url 
    public function paymentSucceeded(Request $request)
    {
        // dd($request->input());
        $value = $request->all();
        $payment_intents = TransactionModel::where('intent_id', $request['data']['object']['payment_intent'])->first();
        if ($value['type'] == 'charge.succeeded') {
            $payment_intents->transaction_id = $request['data']['object']['id'] ?? null;
            $payment_intents->status = $request['data']['object']['status'] == 'succeeded' ? 'processed' : null;
            $payment_intents->exp_month = $request['data']['object']['payment_method_details']['card']['exp_month'] ?? null;
            $payment_intents->exp_year = $request['data']['object']['payment_method_details']['card']['exp_year'] ?? null;
            $payment_intents->last4 = $request['data']['object']['payment_method_details']['card']['last4'] ?? null;
            $payment_intents->save();

            //Push Notification
            // find user
            $user = User::where('stripe_id', $request['data']['object']['customer'])->first();

            //insert notification
            $new_data = [
                'title' => 'Payment Success',
                'message' => $user->first_name . ' ' . $user->last_name . ' Payment succeeded for' . ' ' . $payment_intents->property->name,
                'type' => 'payment_success',
                'user_type' => 'tenant',
                'transaction_id' => $payment_intents->custom_id ?? "",
                'notification_staus' => $user->notification_staus,
            ];
            // not done code for the push notification
            // $this->paymentSuccessPushNotification($new_data,$user);
        } else if ($value['type'] == 'charge.refunded') {
            $payment_intents->transaction_id = $request['data']['object']['id'] ?? null;
            $payment_intents->status = $request['data']['object']['status'] == 'succeeded' ? 'processed' : null;
            $payment_intents->exp_month = $request['data']['object']['payment_method_details']['card']['exp_month'] ?? null;
            $payment_intents->exp_year = $request['data']['object']['payment_method_details']['card']['exp_year'] ?? null;
            $payment_intents->last4 = $request['data']['object']['payment_method_details']['card']['last4'] ?? null;
            $payment_intents->save();

            //Push Notification
            // find user
            $user = User::where('stripe_id', $request['data']['object']['customer'])->first();
            //insert notification
            $new_data = [
                'title' => 'Charge Refund',
                'message' => $user->first_name . ' ' . $user->last_name . ' Payment Refunded for' . ' ' . $payment_intents->property->name,
                'type' => 'charge_refund',
                'user_type' => 'tenant',
                'transaction_id' => $payment_intents->custom_id ?? "",
                'notification_staus' => $user->notification_staus,
            ];
            // $this->chargeRefundPushNotification($new_data,$user);
        } else {
        }

        $this->status = Response::HTTP_OK;
        // return response()->json($this->status, );

        // $orderLog = new Logger('webhook');
        // $orderLog->pushHandler(new StreamHandler(storage_path('logs/webhook.log')), Logger::INFO);
        // $orderLog->info('webbook', $value);
    }
}
