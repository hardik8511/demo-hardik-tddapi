<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManufactorModel extends Model
{
    use HasFactory;
    protected $table = 'manufactor_models';
    protected $fillable = ['model_name','model_description','model_img','manufacture_id'];

    public function manufacture()
    {
        return $this->belongsTo(Manufactor::class,'manufacture_id','id');
    }

    public function images()
    {
        return $this->hasMany(ManufactureModelImages::class,'manufacture_id','id');
    }
}
