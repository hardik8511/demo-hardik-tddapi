<?php

namespace App\GraphQL\Mutations;

use App\Models\Manufactor;
use App\Models\ManufactorModel;
use Closure;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class editModelManufacture extends Mutation
{
    // public function __construct()
    // {
    //     echo "hi"; exit;

    // }

    protected $attributes = [
        'name' => 'editModelManufacture'
    ];

    public function type(): Type
    {
        // echo "hii"; exit;
        return Type::nonNull(GraphQL::type('manufacture_model'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The Id of manufacture model',
            ],
            'manufacture_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The name of manufacture',
                'rules' => ['required', 'exists:manufactors,id'],
            ],
            'model_name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of manufacture',
            ],
            'model_description' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The description of manufacture',
            ],
            'model_img' => [
                'type' => Type::getNullableType(GraphQL::type('Upload')),
                'description' => 'Image Upload',
                'rules' => ['required', 'image', 'max:1500'],
            ],
        ];
    }

    public function resolve($root, array $args)
    {
        $path = null;
        if ($args['model_img']) {
            $path = $args['model_img']->storePublicly('upload');
        }
        $manuFactorData = ManufactorModel::find($args['id']);

        if ($manuFactorData) {

            $manuFactorData->model_name = $args['model_name'];
            $manuFactorData->manufacture_id = $args['manufacture_id'];
            $manuFactorData->model_description = $args['model_description'];
            $manuFactorData->model_img = $path == null ? $manuFactorData->model_img : $path;
            return $manuFactorData;
        }
    }
}
