<?php

namespace App\GraphQL\Mutations;

use App\Models\Manufactor;
use Closure;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class editManuFacture extends Mutation
{
    protected $attributes = [
        'name' => 'Edit The Manufacture'
    ];

    public function type(): Type
    {
        return Type::nonNull(GraphQL::type('manufacture'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'min:3', 'max:50'],
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    public function resolve($root, array $args)
    {
        $manuFactorData = Manufactor::find($args['id']);
        if (!$manuFactorData) {
            return null;
        }

        $manuFactorData->name = $args['name'];
        $manuFactorData->description = $args['description'];
        $manuFactorData->save();
        return $manuFactorData;
    }
}
