<?php

namespace App\GraphQL\Mutations;

use App\Models\Manufactor;
use App\Models\ManufactorModel;
use App\Models\ManufactureModelImages;
use Closure;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class createModelManufactureWithImages extends Mutation
{
    // public function __construct()
    // {
    //     // echo '<pre>'; print_r("hi"); exit;
    // }

    protected $attributes = [
        'name' => 'createModelManufactureWithImages'
    ];

    public function type(): Type
    {
        // echo "hii"; exit;
        return Type::nonNull(GraphQL::type('manufacture_model'));
    }

    public function args(): array
    {
        // echo '<pre>'; print_r("hiiiaa"); exit;
        return [
            'manufacture_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The name of manufacture',
                'rules' => ['required', 'exists:manufactors,id'],
            ],
            'model_name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of manufacture',
            ],
            'model_description' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The description of manufacture',
            ],
            'model_img' => [
                'type' => Type::getNullableType(GraphQL::type('Upload')),
                'description' => 'Image Upload multiple',
                // 'rules' => ['required', 'image', 'max:1500'],
            ],


        ];
    }

    public function resolve($root, array $args)
    {
        $manuFactorData = ManufactorModel::create([
            'model_name' => $args['model_name'],
            'manufacture_id' => $args['manufacture_id'],
            'model_description' =>  $args['model_description'],
            'model_img' =>  "123",
        ]);
        // dd($manuFactorData);
        if ($manuFactorData->id) {
            foreach ($args['model_img'] as $key => $value) {
                $path = $value->store('upload');
                $manufacture_model_img = ManufactureModelImages::create([
                    "manufacture_id" => $manuFactorData->id,
                    "path" =>  $path,
                ]);
            }
        }

        return $manuFactorData;
    }
}
