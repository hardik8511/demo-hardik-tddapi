-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2022 at 12:38 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_multiple_database2`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Reprehenderit exercitationem eos laboriosam. Expedita porro omnis et sequi. Blanditiis molestiae distinctio voluptatem blanditiis. Corporis nam quae saepe magnam autem omnis doloremque.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(2, 'Dolorum quae deleniti natus molestiae maxime et. Minima ut sed quia soluta et. Repellat ea minus voluptatem modi quos ut quae sequi. Et ut at in maiores est.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(3, 'Similique sit voluptas qui. Et rerum hic id ut. Voluptatibus eveniet ut est impedit quos at in quisquam. Soluta consequatur necessitatibus non.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 25),
(4, 'Nisi officiis accusamus totam qui. Quis aut cupiditate voluptatum qui consequatur illo. Ut error quos sequi accusantium debitis fugiat itaque.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(5, 'Ut et ipsa dignissimos id voluptatibus. Id consequatur sequi nulla rerum odit quod. Magnam porro aut expedita sint qui.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(6, 'Qui odit adipisci quis labore qui. Molestias earum quia unde aut dolores expedita. Velit reiciendis sunt eveniet. Aspernatur odio quod voluptatibus accusamus est assumenda.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(7, 'Et occaecati reiciendis deleniti aut necessitatibus dicta. Eius facere ut id dicta aut rem. Aut non commodi vitae impedit omnis nesciunt optio.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(8, 'Cupiditate unde et molestias repellat culpa ut. Voluptatem odio voluptatem perferendis molestiae maxime ad vitae voluptatem. Magnam voluptatem consectetur fugiat possimus.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(9, 'Fugit qui et rerum. Veniam cupiditate at et illum magnam. Facere recusandae est reiciendis enim rem dicta.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1),
(10, 'Eos aut non ut in. Id libero sapiente maiores in. Fugiat dicta et neque aliquid quam ratione non animi.', '2022-05-12 04:50:36', '2022-05-12 04:50:36', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
