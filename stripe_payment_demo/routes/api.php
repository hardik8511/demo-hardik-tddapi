<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\StripePaymentController as ApiStripePaymentController;
use App\Http\Controllers\StripePaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::post("create-customer", [ApiStripePaymentController::class, 'createCustomerManually'])->name('createCustomer');
    Route::post('createEphemeralKey', [ApiStripePaymentController::class, 'createEphemeralKey'])->name('createEphemeralKey');
    Route::post('createPaymentIntent', [ApiStripePaymentController::class, 'createPaymentIntent'])->name('createPaymentIntent');
    Route::post('paymentSucceeded', [ApiStripePaymentController::class, 'paymentSucceeded'])->name('paymentSucceeded');
});
// create the test cases, and webhook for stripe payment gateway 
//attend the session 

Route::post("user_login", [AuthController::class, 'userLogin']);
// Route::get('payment-sucess',[StripePaymentController::class,'   '])->name('payment_sucess');
// payment-sucess