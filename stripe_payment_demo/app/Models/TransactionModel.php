<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    use HasFactory;
    protected $table = 'transaction_models';
    protected $fillable = ['user_id', 'intent_id', 'amount', 'transaction_id', 'exp_month', 'exp_year', 'last4', 'payment_type', 'status', 'type'];
}
